<?xml version="1.0"?>
<app>

<category>
Icons
</category>

<name>  
Papirus antiX
</name>

<description>  
Full version of Papirus icons for antiX Linux
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
papiris-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
papiris-antix
</uninstall_package_names>
</app>
