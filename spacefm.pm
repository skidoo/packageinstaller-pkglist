<?xml version="1.0"?>
<app>

<category>
File Managers
</category>

<name>  
SpaceFM
</name>

<description>  
Multi-panel tabbed file and desktop manager
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
spacefm
udevil
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
spacefm
</uninstall_package_names>
</app>