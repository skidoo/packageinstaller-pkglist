<?xml version="1.0"?>
<app>

<category>
Misc
</category>

<name>
Google Earth Pro
</name>

<description>
Google Earth Globe
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
echo "deb [arch=amd64] http://dl.google.com/linux/earth/deb/ stable main">/etc/apt/sources.list.d/mxpitemp.list
apt-get update
</preinstall>

<install_package_names>
google-earth-pro-stable
</install_package_names>


<postinstall>
rm /etc/apt/sources.list.d/mxpitemp.list
</postinstall>


<uninstall_package_names>
google-earth-pro-stable
</uninstall_package_names>
</app>
