<?xml version="1.0"?>
<app>

<category>
Browser
</category>

<name>
Waterfox G3
</name>

<description>
Alternative mozilla-based browser with all WebExtensions
</description>

<installable>
64
</installable>

<screenshot>https://www.waterfoxproject.org/media/img/waterfox/products/desktop/waterfox-browser.40990c516643.svg</screenshot>

<preinstall>
</preinstall>

<install_package_names>
waterfox-g3-kpe
</install_package_names>


<postinstall>
if [ "$(locale |grep LANG|cut -d= -f2 |cut -d_ -f1)" != "en" ]; then
apt-get install waterfox-g3-i18n-$(locale |grep LANG|cut -d= -f2 |cut -d_ -f1)
fi
</postinstall>


<uninstall_package_names>
waterfox-g3-kpe
</uninstall_package_names>
</app>
