<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>
Mullvad VPN
</name>

<description>
Mullvad VPN and sysVinit scripts
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
mullvadvpn-sysvinit-compat
mullvadvpn-downloader-installer
</install_package_names>

<postinstall>
# run Mullvad installer
INSTALL=/usr/share/mullvad-downloader-installer/install_mullvadvpn.sh
if [ -x $INSTALL ]; then
/usr/share/mullvad-downloader-installer/install_mullvadvpn.sh
fi

</postinstall>

<uninstall_package_names>
mullvadvpn-sysvinit-compat
mullvadvpn-downloader-installer
mullvad-vpn
</uninstall_package_names>
</app>
