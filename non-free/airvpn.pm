<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>
AirVPN
</name>

<description>
AirVPN - Eddie - OpenVPN GUI with additional user-friendly features
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
# install airvpn
# prepare temp dir and files
DIR=$(mktemp -d /tmp/tempdir-airvpn-keyring.XXXXXXXXXXXXX)
chmod 700 $DIR
ASC=$DIR/airvpn.asc
GPG=$DIR/airvpn.gpg
KBX=$DIR/airvpn.kbx
AKR=/etc/apt/trusted.gpg.d/airvpn-archive-keyring.gpg
LST=/etc/apt/sources.list.d/airvpn-stable.list
SRC="deb http://eddie.website/repository/apt stable main"
INR=$DIR/InRelease

# prepare tidy-up
tidy_up() { rm -r /tmp/tempdir-airvpn-keyring.* 2>/dev/null; }
trap tidy_up EXIT

echo "Get AirVPN repository keyring"
curl -sq -o $ASC -RLJ https://eddie.website/repository/keys/eddie_maintainer_gpg.key
[ -f "$ASC" ] || { echo "Fatal: Failed to fetch AirVPN repository signing key"; exit 1; }
gpg --no-default-keyring --homedir=$DIR --keyring $KBX --import  $ASC 2>/dev/null

printf "Check Repository InRelease signature\n\n"
curl -sq -RLJ -o $INR http://eddie.website/repository/apt/dists/stable/InRelease
[ -f "$INR" ] || { echo "Fatal: Failed to fetch AirVPN InRelease"; exit 2; }

# E2S to handle ampersand redirects
E2S="$(printf '2\x3e\x261')"
CMD="env LC_ALL=C gpg --verify  $INR $E2S | sed -n 's/^gpg:.*using.*key //p' | grep -ioE '[A-F0-9]{40}'"
INS="$(eval $CMD)"
gpg --no-default-keyring --homedir=$DIR --keyring $KBX --output $GPG --export $INS 2>/dev/null
[ -f "$GPG" ] || { echo "Fatal: Repo signing key error "; exit 3; }
printf "Setup Repository Archive Key: \n"
CMD="env LC_ALL=C gpg --no-default-keyring --homedir=$DIR --keyring $KBX --list-keys $INS $E2S"
printf "$(eval $CMD)\n\n"

cp $GPG $AKR

if ! sed  -re '/^[[:space:]]*#/d; s/#.*//; /^[[:space:]]*deb[[:space:]]+/!d' /etc/apt/sources.list{,.d/*.list} \
| grep -sqE "eddie.website/repository/apt[[:space:]]+stable[[:space:]]+main" ; then
echo "Setup AirVPN repository apt source list"
echo "$SRC" | tee $LST
fi
printf "Refresh package list ...\n\n"
apt-get update

</preinstall>

<install_package_names>
eddie-ui
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
eddie-ui
</uninstall_package_names>

<postuninstall>
# purge AirVPN-client
if dpkg-query -f '${db:Status-Abbrev}' -W eddie-ui  2>/dev/null 1>/dev/null; then
echo "Postuninstall: Purge AirVPN"
apt-get -y remove --purge eddie-ui
fi
rm -f /etc/apt/sources.list.d/airvpn-stable.list 2>/dev/null
rm -f /etc/apt/trusted.gpg.d/airvpn-archive-keyring.gpg 2>/dev/null
</postuninstall>

</app>
