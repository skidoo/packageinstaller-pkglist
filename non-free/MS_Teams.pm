<?xml version="1.0"?>
<app>

<category>
Messaging
</category>

<name>
MS-Teams
</name>

<description>
Microsoft Teams
</description>

<installable>
64
</installable>

<screenshot></screenshot>

<preinstall>
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/microsoft.gpg
install -o root -g root -m 644 /tmp/microsoft.gpg /etc/apt/trusted.gpg.d/
echo "deb [arch=amd64] https://packages.microsoft.com/repos/ms-teams stable main">/etc/apt/sources.list.d/mxpitemp.list
apt-get update
</preinstall>

<install_package_names>
apt-transport-https
teams
</install_package_names>


<postinstall>
rm /etc/apt/sources.list.d/mxpitemp.list
</postinstall>


<uninstall_package_names>
teams
</uninstall_package_names>
</app>
