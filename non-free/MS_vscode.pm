<?xml version="1.0"?>
<app>

<category>
Development
</category>

<name>
MS-VSCode
</name>

<description>
Microsoft Visual Studio Code
</description>

<installable>
64
</installable>

<screenshot>https://code.visualstudio.com/assets/home/home-screenshot-linux-lg.png</screenshot>

<preinstall>
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/microsoft.gpg
install -o root -g root -m 644 /tmp/microsoft.gpg /etc/apt/trusted.gpg.d/
echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main">/etc/apt/sources.list.d/vscode.list
apt-get update
</preinstall>

<install_package_names>
apt-transport-https
code
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
code
</uninstall_package_names>
</app>
