<?xml version="1.0"?>
<app>

<category>
Office
</category>

<name>
SoftMaker FreeOffice 2018
</name>

<description>
SoftMaker FreeOffice 2018 - fast, compatible and reliable office suite
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

echo "---------------------"
echo "FreeOffice Installer "
echo "---------------------"

# softmaker repo-key
KEY="
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1

mQENBFpOLj4BCAC8XZffd/y5zonHkCFswGagKUO4dYo0VTpHouHo3aShVpw4J/Xh
EHzmMuFN7gAt1wa/mnp8k3gZkj1SvcY/nnFxGE/8SL6nuUzZY8yUbuTP8xkp31U+
kQUpBCefip2ZHdFg3bYMGkWDDVm0cMo7nD1rznNN8GSC8UU8Y6YFrodUc/WIpgEo
GqbS3opFgwMBdrt/+wWTz4/+N5tC4HQXG2IMoC4fodydQu+6iN+cgZVibpJeRybW
rQkmMJAUydBy+o3CRhIE8yFnaJ4mWZieZQy78sWw5bZPFHjg3kOEIxSncFaYHfWY
otRGbQlenA4ij9pqyAJtqBT4LqBET2FHqcmJABEBAAG0RVNvZnRNYWtlciByZXBv
c2l0b3J5IChHUEcga2V5IGZvciBzaWduaW5nIGZpbGVzKSA8aW5mb0Bzb2Z0bWFr
ZXIuY29tPokBOAQTAQIAIgUCWk4uPgIbAwYLCQgHAwIGFQgCCQoLBBYCAwECHgEC
F4AACgkQNBPamKo+f14imQf+PwJOZwTS+3zVQRjBgPjtxSdsOcONnjNhvYoe3N+v
NQZZMOlksndviM7AMB7kcYV5NWiawYvvbg7knpsMdgMzvToB0CpKQ/K7oy8kAq0O
HdtA1HwV/23ExH+EcAtCZnzD8YopRXlcoN6hGG4GkDzSf/Rnj4b6ImtKVBcy0R43
BbbL6cMFJj3Gw51MoxR9ZXBV4job+9T3pt7rCb1mnq4x8ocCLvtT7vgs0QnwC+Pb
PgHXHHTYKcFeoZf3IrGx9ZcMKCbShC0LQv5Kn8PiQZXgIf24RQvp4ib1XO1lY38W
3q4VvxvJIxkmbjGnADwUaESz/hP8I0j5OIVM5Uefb/k0oQ==
=YG9D
-----END PGP PUBLIC KEY BLOCK-----
"
echo "adding Softmaker repository key: 0x3413DA98AA3E7F5E"
echo "$KEY" | gpg --keyid-format="0xlong"  2>/dev/null

# curl -RLJ https://shop.softmaker.com/repo/linux-repo-public.key | apt-key add -
# echo "$KEY" | ( apt-key add - ) 2>/dev/null

rm /etc/apt/trusted.gpg.d/softmaker-keyring.gpg 2>/dev/null
echo "$KEY" | sudo gpg -o /etc/apt/trusted.gpg.d/softmaker-keyring.gpg --dearmor 2>/dev/null

echo "adding Softmaker repository  /etc/apt/sources.list.d/softmaker.list:"
echo "deb http://shop.softmaker.com/repo/apt wheezy non-free"

rm /etc/apt/sources.list.d/softmaker.list 2>/dev/null
echo "deb http://shop.softmaker.com/repo/apt wheezy non-free" > /etc/apt/sources.list.d/softmaker.list

apt-get update

</preinstall>

<install_package_names>
softmaker-freeoffice-2018
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
softmaker-freeoffice-2018
</uninstall_package_names>

<postuninstall>

echo "removing Softmaker repository key"
apt-key del 0x3413DA98AA3E7F5E

echo "removing Softmaker repository  /etc/apt/sources.list.d/softmaker.list"
rm /etc/apt/sources.list.d/softmaker.list
echo "DONE"

</postuninstall>

</app>
