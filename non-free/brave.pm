<?xml version="1.0"?>
<app>

<category>
Browser
</category>

<name>
Brave
</name>

<description>
Latest Brave browser
</description>

<installable>
64
</installable>

<screenshot> </screenshot>

<preinstall>
URL="https://brave-browser-apt-release.s3.brave.com"
ASC="${URL}/brave-core.asc"
TKR="/etc/apt/trusted.gpg.d/brave-browser-release.gpg"
SRC="/etc/apt/sources.list.d/brave-browser-release.list"
VER="$(sed -n  's/^VERSION_CODENAME=//p' /etc/os-release)"
case $(cat /etc/debian_version) in
8*) VER=jessie   ;;
9*) VER=stretch  ;;
10*) VER=buster   ;;
11*) VER=bullseye ;;
esac

DEB="deb [arch=amd64] ${URL}/ $VER main"

curl -s $ASC | ( apt-key --keyring $TKR add - 2>/dev/null )
echo "$DEB" | tee $SRC
apt-get update

</preinstall>

<install_package_names>
brave-browser
</install_package_names>

<postinstall>
# fix sandbox

SYSCTL=/etc/sysctl.d/10-securized-yama-scope.conf
echo "kernel.yama.ptrace_scope = 1" > $SYSCTL
if [ -f $SYSCTL ]; then  sysctl --load $SYSCTL; fi

</postinstall>

<uninstall_package_names>
brave-browser
brave-keyring
</uninstall_package_names>

<postuninstall>

apt-get -y remove --purge brave-browser brave-browser
SRC=/etc/apt/sources.list.d/brave-browser-release.list
TKR="/etc/apt/trusted.gpg.d/brave-browser-release.gpg"
rm -f $SRC 2>/dev/null
rm -f $TKR 2>/dev/null
apt-get update
</postuninstall>

</app>
