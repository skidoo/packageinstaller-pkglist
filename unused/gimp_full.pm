<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>  
GIMP Full
</name>

<description>  
advanced picture editor- installs GIMP, help and plugins
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gimp
gimp-data
gimp-data-extras
gimp-cbmplugs
gimp-dcraw
gimp-dds 
gimp-gap 
gimp-gluas 
gimp-gmic 
gimp-lensfun  
gimp-texturize 
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gimp
gimp-data
gimp-data-extras
gimp-cbmplugs
gimp-dcraw
gimp-dds 
gimp-gap 
gimp-gluas 
gimp-gmic 
gimp-lensfun  
gimp-texturize 
</uninstall_package_names>
</app>