<?xml version="1.0"?>
<app>

<category>
Themes
</category>

<name>  
Adapta Gtk Theme
</name>

<description>  
Modern flat Gtk theme in light and dark variants, with widgets having neon blue highlights
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
adapta-gtk-theme
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
adapta-gtk-theme
</uninstall_package_names>
</app>
