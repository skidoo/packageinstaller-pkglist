<?xml version="1.0"?>
<app>

<category>
Desktop Environment
</category>

<name>  
LXDE
</name>

<description>  
Basic install of LXDE
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
lxde
lxpolkit
gvfs
elogind
obconf
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
lxde
lxpolkit
obconf
</uninstall_package_names>
</app>