<?xml version="1.0"?>
<app>

<category>
Desktop Environment
</category>

<name>  
Gnome
</name>

<description>  
Install Gnome
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
gnome
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
gnome
</uninstall_package_names>

</app>
