<?xml version="1.0"?>
<app>

<category>
Office
</category>

<name>  
Calibre
</name>

<description>  
e-book library management application
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
calibre
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
calibre
</uninstall_package_names>
</app>