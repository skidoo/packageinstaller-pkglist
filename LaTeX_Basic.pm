<?xml version="1.0"?>
<app>

<category>
LaTeX
</category>

<name>  
LaTeX-Basic
</name>

<description>  
Basic LaTeX installation
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
texlive
texlive-base
texlive-latex-base
texlive-latex-recommended
texlive-fonts-recommended
preview-latex-style
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
texlive
texlive-base
texlive-latex-base
texlive-latex-recommended
texlive-fonts-recommended
preview-latex-style
</uninstall_package_names>

</app>
