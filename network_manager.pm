<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>  
Network Manager
</name>

<description>  
System network service to manage network devices and connections
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
network-manager
network-manager-gnome
network-manager-openconnect 
network-manager-openconnect-gnome
network-manager-openvpn
network-manager-openvpn-gnome 
network-manager-pptp
network-manager-vpnc
mobile-broadband-provider-info
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
network-manager
network-manager-gnome
network-manager-openconnect 
network-manager-openconnect-gnome
network-manager-openvpn
network-manager-openvpn-gnome 
network-manager-pptp
network-manager-vpnc
mobile-broadband-provider-info
</uninstall_package_names>
</app>