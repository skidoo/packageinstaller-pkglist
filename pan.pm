<?xml version="1.0"?>
<app>

<category>
Newsreader
</category>

<name>  
Pan
</name>

<description>  
Gnome Usenet newsreader
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
pan
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
pan
</uninstall_package_names>
</app>