<?xml version="1.0"?>
<app>

<category>
Docks
</category>

<name>  
Plank
</name>

<description>  
Simple but good looking dock
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
plank
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
plank
</uninstall_package_names>
</app>