<?xml version="1.0"?>
<app>

<category>
Graphics
</category>

<name>  
Scribus
</name>

<description>  
Desktop page layout program
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
scribus
scribus-template
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
scribus
scribus-template
</uninstall_package_names>
</app>