<?xml version="1.0"?>
<app>

<category>
LaTeX
</category>

<name>  
LaTeX-Texmaker
</name>

<description>  
LaTeX editor
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
texmaker
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
texmaker
</uninstall_package_names>

</app>
