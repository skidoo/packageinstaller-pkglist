<?xml version="1.0"?>
<app>

<category>
Office
</category>

<name>  
GnuCash
</name>

<description>  
Personal and small-business financial-accounting software
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gnucash
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gnucash
</uninstall_package_names>
</app>