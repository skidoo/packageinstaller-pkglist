<?xml version="1.0"?>
<app>

<category>
Kernel
</category>

<name>  
Kernel-antiX_64bit_meltdown_patched
</name>

<description>  
antiX Kernel 64 bit Meltdown, Spectre and Foreshadow patched LTS (4.9.240)
</description>

<installable>
64
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
linux-image-4.9.240-antix.1-amd64-smp
linux-headers-4.9.240-antix.1-amd64-smp
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-4.9.240-antix.1-amd64-smp
linux-headers-4.9.240-antix.1-amd64-smp
</uninstall_package_names>

</app>
