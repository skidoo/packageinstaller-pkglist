<?xml version="1.0"?>
<app>

<category>
Media Converter
</category>

<name>  
Asunder
</name>

<description>  
Graphical audio CD ripper and encoder
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
asunder
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
asunder
</uninstall_package_names>
</app>