<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>  
MuseScore
</name>

<description>  
Music notation software 
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
musescore
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
musescore
</uninstall_package_names>
</app>