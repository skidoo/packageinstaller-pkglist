<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>  
MPD Music Player Daemon
</name>

<description>  
Flexible, powerful, server-side application for playing music
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
mpd
mpc
mpdscribble
ncmpcpp
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
mpd
mpc
mpdscribble
ncmpcpp
</uninstall_package_names>
</app>