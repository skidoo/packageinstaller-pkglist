<?xml version="1.0"?>
<app>

<category>
Torrent
</category>

<name>  
rTorrent
</name>

<description>  
Ncurses BitTorrent client
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
rtorrent
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
rtorrent
</uninstall_package_names>
</app>