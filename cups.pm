<?xml version="1.0"?>
<app>

<category>
Printing
</category>

<name>  
CUPS
</name>

<description>  
Printing system
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
cups
cups-filters
cups-pdf
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
cups
cups-filters
cups-pdf
</uninstall_package_names>
</app>