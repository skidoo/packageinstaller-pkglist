<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>  
ModemManager
</name>

<description>  
Controls mobile broadband (2G/3G/4G) devices and connections
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
mobile-broadband-provider-info
modemmanager
usb-modeswitch
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
mobile-broadband-provider-info
modemmanager
usb-modeswitch
</uninstall_package_names>
</app>