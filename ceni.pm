<?xml version="1.0"?>
<app>

<category>
Network
</category>

<name>  
Ceni
</name>

<description>  
A Curses user interface for configuring network interfaces via ifupdown
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
ceni
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
ceni
</uninstall_package_names>
</app>