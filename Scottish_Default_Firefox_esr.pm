<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Scottish_Default_Firefox_esr
</name>

<description>  
Scottish Gaelic localisation of default installed Firefox ESR
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
firefox-esr-l10n-gd
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
firefox-esr-l10n-gd
</uninstall_package_names>

</app>
