<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>  
MOC
</name>

<description>  
Console audio player designed to be powerful and easy to use
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
moc
moc-ffmpeg-plugin
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
moc
moc-ffmpeg-plugin
</uninstall_package_names>
</app>