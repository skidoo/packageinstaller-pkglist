<?xml version="1.0"?>
<app>

<category>
Browser
</category>

<name>  
Chromium
</name>

<description>  
Latest Chromium browser and language pack
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
chromium
chromium-l10n
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
chromium
chromium-l10n
</uninstall_package_names>
</app>