<?xml version="1.0"?>
<app>


<category>
Screencast
</category>

<name>  
recordmydesktop
</name>

<description>  
Desktop session recorder
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gtk-recordmydesktop
recordmydesktop
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gtk-recordmydesktop
recordmydesktop
</uninstall_package_names>

</app>
