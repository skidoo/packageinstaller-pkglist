<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>  
Audacious
</name>

<description>  
Lightweight winamp/xmms like audio player
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
audacious
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
audacious
</uninstall_package_names>
</app>