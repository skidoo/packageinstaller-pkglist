<?xml version="1.0"?>
<app>

<category>
Torrent
</category>

<name>  
Transmission
</name>

<description>  
Lightweight BitTorrent client
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
transmission
transmission-cli
transmission-gtk
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
transmission
transmission-cli
transmission-gtk
</uninstall_package_names>
</app>