<?xml version="1.0"?>
<app>

<category>
Office
</category>

<name>
MuPDF
</name>

<description>
Lightweight and fast PDF, XPS, and E-book viewer
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
mupdf
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
mupdf
</uninstall_package_names>
</app>
