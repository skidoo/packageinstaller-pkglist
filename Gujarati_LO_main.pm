<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Gujarati_LO_main
</name>

<description>  
Gujarati LibreOffice Language Meta-Package
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-gu
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-l10n-gu
libreoffice-gtk3
</uninstall_package_names>

</app>
