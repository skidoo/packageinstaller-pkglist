<?xml version="1.0"?>
<app>

<category>
Browser
</category>

<name>  
Seamonkey
</name>

<description>  
Latest Seamonkey packaged by MX Community
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
seamonkey
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
seamonkey
</uninstall_package_names>
</app>