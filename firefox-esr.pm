<?xml version="1.0"?>
<app>

<category>
Browser
</category>

<name>
Firefox-ESR
</name>

<description>
Firefox Extended Support Release (ESR)
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
firefox-esr
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
firefox-esr
</uninstall_package_names>
</app>
