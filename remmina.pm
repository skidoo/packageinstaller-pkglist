<?xml version="1.0"?>
<app>

<category>
Remote Access
</category>

<name> 
Remmina
</name>

<description>  
Remote desktop client written in GTK+
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
remmina
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
remmina
</uninstall_package_names>
</app>