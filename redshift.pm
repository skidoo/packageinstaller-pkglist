<?xml version="1.0"?>
<app>

<category>
Utility
</category>

<name>
Redshift
</name>

<description>
Transition monitor color temperature by time of day
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
redshift
redshift-gtk
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
redshift
redshift-gtk
</uninstall_package_names>
</app>
