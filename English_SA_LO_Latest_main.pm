<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
English_SA_LO_Latest_main
</name>

<description>  
SA English LibreOffice Language Meta-Package
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "deb http://deb.debian.org/debian buster-backports main contrib non-free">/etc/apt/sources.list.d/pitemp.list
apt-get update
</preinstall>

<install_package_names>
-t buster-backports libreoffice-l10n-en-za
</install_package_names>

<postinstall>
rm /etc/apt/sources.list.d/pitemp.list
apt-get update
</postinstall>

<uninstall_package_names>
libreoffice-l10n-en-za
</uninstall_package_names>

</app>
