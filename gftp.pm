<?xml version="1.0"?>
<app>

<category>
FTP
</category>

<name>  
gftp
</name>

<description>  
Multithreaded FTP client
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gftp-gtk
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gftp-gtk
</uninstall_package_names>
</app>