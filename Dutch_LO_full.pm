<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Dutch_LO_full
</name>

<description>  
Dutch LibreOffice Language Meta-Package
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-nl
libreoffice-help-nl
libreoffice-gtk3
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
libreoffice-base-core
libreoffice-calc
libreoffice-draw
libreoffice-impress
libreoffice-math
libreoffice-writer
libreoffice-base
libreoffice-l10n-nl
libreoffice-help-nl
libreoffice-gtk3
</uninstall_package_names>

</app>
