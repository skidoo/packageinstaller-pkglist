<?xml version="1.0"?>
<app>

<category>
Desktop Environment
</category>

<name>  
Gnome-core
</name>

<description>  
Very minimal install of Gnome
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
gnome-core
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
gnome-core
</uninstall_package_names>
</app>