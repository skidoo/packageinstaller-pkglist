<?xml version="1.0"?>
<app>

<category>
Window Managers
</category>

<name>  
IceWM
</name>

<description>  
Lightweight window manager
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
icewm
icewm-common
icewm-themes-antix
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
icewm
icewm-common
icewm-themes-antix
</uninstall_package_names>
</app>