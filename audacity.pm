<?xml version="1.0"?>
<app>

<category>
Audio
</category>

<name>  
Audacity
</name>

<description>  
Multi-track audio editor
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>

</preinstall>

<install_package_names>
audacity
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
audacity
</uninstall_package_names>
</app>