<?xml version="1.0"?>
<app>

<category>
Kernel
</category>

<name>  
Kernel-Debian_32bit
</name>

<description>  
Fallback Debian 4.19 32bit linux kernel 
</description>

<installable>
32
</installable>

<screenshot>none</screenshot>

<preinstall>
</preinstall>

<install_package_names>
linux-image-4.19.0-13-686
linux-headers-4.19.0-13-686
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
linux-image-4.19.0-13-686
linux-headers-4.19.0-13-686
</uninstall_package_names>

</app>
