<?xml version="1.0"?>
<app>

<category>
Browser
</category>

<name>
Tor Browser Launcher
</name>

<description>
Helps download and run the Tor Browser Bundle
</description>

<installable>
all
</installable>

<preinstall>

</preinstall>

<install_package_names>
torbrowser-launcher
tor
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
torbrowser-launcher
tor
</uninstall_package_names>
</app>
